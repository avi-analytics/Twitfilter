try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup
from distutils.core import Extension

 params = {
     "name"="",
     "version"="1.0",
     "author" = "Avinash M. Tripathi",
     "author_email" = "avinashauction@gmail.com",
#     "requires" = "[]",
#     "provides" = "[]",
#     "obsoletes" = "[]",
#     "scripts" = "[]",
#     "description" = "",
#     "license" = "BSD 3 clause",
#     "classification" = "",
#     "url" = "",
#     "py_modules" = "",
#     "packages" = "",
#      package_dir={'mypkg': 'src/mypkg'},
#      package_data={'mypkg': ['data/*.dat']},
#     "Ext_modules"= "[]"
     }
setup(**params)
        
